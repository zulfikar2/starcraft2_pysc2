<div align="center">
  <a href="https://www.youtube.com/watch?v=-fKUyT14G-8"
     target="_blank">
    <img src="http://img.youtube.com/vi/-fKUyT14G-8/0.jpg"
         alt="DeepMind open source PySC2 toolset for Starcraft II"
         width="240" height="180" border="10" />
  </a>
  <a href="https://www.youtube.com/watch?v=6L448yg0Sm0"
     target="_blank">
    <img src="http://img.youtube.com/vi/6L448yg0Sm0/0.jpg"
         alt="StarCraft II 'mini games' for AI research"
         width="240" height="180" border="10" />
  </a>
  <a href="https://www.youtube.com/watch?v=WEOzide5XFc"
     target="_blank">
    <img src="http://img.youtube.com/vi/WEOzide5XFc/0.jpg"
         alt="Trained and untrained agents play StarCraft II 'mini-game'"
         width="240" height="180" border="10" />
  </a>
</div>

# PySC2 Ladder Bot
This is a StarCraft 2 bot using DeepMind's [PySC2 - StarCraft II Learning Environment](https://github.com/deepmind/pysc2) that has the ability to integrate with the [LadderManager](https://github.com/Cryptyc/Sc2LadderServer) so that it can run against other bots on the [SC2 AI Ladder](http://sc2ai.net).

This bot can be run either locally against a computer opponent, or through the [LadderManager](https://github.com/Cryptyc/Sc2LadderServer).

Based off the [PySC2 Ladder Bot Example](https://github.com/Archiatrus/pysc2-ladderbot)

## Requirements
* [PySC2 - StarCraft II Learning Environment](https://github.com/deepmind/pysc2)

## Usage

### Run a basic game 
```
python run.py
```
You can modify run.py to load your own bot or change the computer opponent.

### Run a LadderManager game
If you want to run LadderManager yourself to test the bot against other ladder bots, you must first download and compile [LadderManager](https://github.com/Cryptyc/Sc2LadderServer). Then extract pysc2-ladderbot into LadderManager/Bots/pysc2bot and add the following to LadderManager/Bots/LadderBots.json:
```
"PacifistAres": {
			"Race": "Zerg",
			"Type": "Python",
			"RootPath": "C:/Ladder/Bots/pysc2bot/",
			"FileName": "run.py"
		},
```
You should now be able to configure LadderManager to start a game with "pysc2bot" as one of the opponents (by modifying LadderManager/matchupList).

## Additional resources
For more info check out the [sc2ai.net wiki](http://wiki.sc2ai.net/Main_Page). If you have questions, this [Discord](https://discord.gg/qTZ65sh) is the best place to ask them.

## License

Copyright © 2019 Zulfikar Yusufali

Licensed under the [MIT license](https://gitlab.com/zulfikar2/starcraft2_pysc2/blob/master/LICENSE).
