import random
import math

import numpy as np
import pandas as pd

from pysc2.lib import actions, features, units
from pysc2.agents import base_agent

#Code Shorteners
_SELECT_POINT = actions.FUNCTIONS.select_point.id
_SELECT_ARMY = actions.FUNCTIONS.select_army.id
_ATTACK_MINIMAP = actions.FUNCTIONS.Attack_minimap.id

_TRAIN_OVERLORD = actions.FUNCTIONS.Train_Overlord_quick.id
_TRAIN_DRONE = actions.FUNCTIONS.Train_Drone_quick.id
_TRAIN_ZERGLING = actions.FUNCTIONS.Train_Zergling_quick.id

_BUILD_HATCHERY = actions.FUNCTIONS.Build_Hatchery_screen.id
_BUILD_SPAWNINGPOOL = actions.FUNCTIONS.Build_SpawningPool_screen.id

ACTION_NOTHING = 'nothing'

ACTION_TRAIN_DRONE = 'train-drone'
ACTION_TRAIN_OVERLORD = 'train-overlord'
ACTION_TRAIN_ZERGLING = 'train-zerglings'

ACTION_BUILD_SPAWNINGPOOL = 'build-pool'

ACTION_SELECT_LARVAE = 'select-larvae'
ACTION_SELECT_DRONE = 'select-drone'
ACTION_SELECT_ARMY = 'select-army'
ACTION_ATTACK = 'attack'

smart_actions = [
  ACTION_NOTHING,

  ACTION_TRAIN_DRONE,
  ACTION_TRAIN_OVERLORD,
  ACTION_TRAIN_ZERGLING,

  ACTION_BUILD_SPAWNINGPOOL,

  ACTION_SELECT_LARVAE,
  ACTION_SELECT_DRONE,
  ACTION_SELECT_ARMY,
  ACTION_ATTACK,
]

#Rewards
KILL_UNIT_REWARD = 0.2
KILL_BUILDING_REWARD = 0.5
#COLLECT_MINS_REWARD = 0.01
#LOST_HP_REWARD = -0.05
ARMY_COUNT_REWARD = 0.08

class PacifistAres(base_agent.BaseAgent):

  #init
  def __init__(self):
    super(PacifistAres, self).__init__()

    self.attack_coords = None
    self.qlearn = QLearningTable(actions=list(range(len(smart_actions))))

    self.previous_killed_unit_score = 0
    self.previous_killed_building_score = 0
    #self.previous_collected_mins = 0
    #self.previous_lost_hp = 0
    self.previous_army_count = 0

    self.previous_action = None
    self.previous_state = None
  
  #game loop
  def step(self, obs):
      super(PacifistAres, self).step(obs)

      if obs.first():
        player_y, player_x = (obs.observation.feature_minimap.player_relative == 
          features.PlayerRelative.SELF).nonzero()
        xmean = player_x.mean()
        ymean = player_y.mean()

        if xmean <= 31 and ymean <= 31:
          self.attack_coords = (52,52)
        else:
          self.attack_coords = (12,16)

      pool_y, pool_x = (obs.observation.feature_screen.unit_type ==
        _BUILD_SPAWNINGPOOL).nonzero()
      pool_count = 1 if pool_y.any() else 0

      supply_limit = obs.observation.player.food_cap
      army_supply = obs.observation.player.food_used

      killed_unit_score = obs.observation.score_cumulative.killed_value_units
      killed_building_score = obs.observation.score_cumulative.killed_value_structures
      #collected_mins = obs.observation.score_cumulative.collected_minerals
      #lost_hp_score = obs.observation.score_by_vital[1][0]
      army_count = obs.observation.player.army_count

      current_state = [
        pool_count,
        supply_limit,
        army_supply,
      ]
      if self.previous_action is not None:
        reward = 0

        if killed_unit_score > self.previous_killed_unit_score:
          reward += KILL_UNIT_REWARD
        
        if killed_building_score > self.previous_killed_building_score:
          reward += KILL_BUILDING_REWARD
        
        #if collected_mins > self.previous_collected_mins:
         #reward += COLLECT_MINS_REWARD

        #if lost_hp_score > self.previous_lost_hp:
          #reward += LOST_HP_REWARD

        if army_count > self.previous_army_count:
          reward += ARMY_COUNT_REWARD

        self.qlearn.learn(str(self.previous_state), self.previous_action, reward, str(current_state))

      r1_action = self.qlearn.choose_action(str(current_state))
      smart_action = smart_actions[r1_action]

      self.previous_killed_unit_score = killed_unit_score
      self.previous_killed_building_score = killed_building_score
      #self.previous_collected_mins = collected_mins
      #self.previous_lost_hp = lost_hp_score
      self.previous_army_count = army_count
      self.previous_state = current_state
      self.previous_action = r1_action

      if smart_action == ACTION_NOTHING:
        return actions.FUNCTIONS.no_op()

      elif smart_action == ACTION_TRAIN_DRONE:
        if self.can_do(obs, _TRAIN_DRONE):
          return actions.FUNCTIONS.Train_Drone_quick("now")

      elif smart_action == ACTION_TRAIN_OVERLORD:
        if self.can_do(obs, _TRAIN_OVERLORD):
          return actions.FUNCTIONS.Train_Overlord_quick("now")

      elif smart_action == ACTION_TRAIN_ZERGLING:
        if self.can_do(obs, _TRAIN_ZERGLING):
          return actions.FUNCTIONS.Train_Zergling_quick("now")

      elif smart_action == ACTION_BUILD_SPAWNINGPOOL:
        if self.can_do(obs, _BUILD_SPAWNINGPOOL):
          x = random.randint(0, 83)
          y = random.randint(0, 83)
          return actions.FUNCTIONS.Build_SpawningPool_screen("now", (x, y))

      elif smart_action == ACTION_SELECT_LARVAE:
        #Get a list of all larvae
        larvae = self.get_units_by_type(obs, units.Zerg.Larva)
        #Select one of the larvae
        if len(larvae) > 0:
          larva = random.choice(larvae)
          #Select Larva
          return actions.FUNCTIONS.select_point("select_all_type", (larva.x, larva.y))

      elif smart_action == ACTION_SELECT_DRONE:
        #Get a list of all drones
        drones = self.get_units_by_type(obs, units.Zerg.Drone)
        #Select one of the drones
        if len(drones) > 0:
          drone = random.choice(drones)
          #Select drone
          return actions.FUNCTIONS.select_point("select_all_type", (drone.x, drone.y))

      elif smart_action == ACTION_SELECT_ARMY:
        if self.can_do(obs, _SELECT_ARMY):
          return actions.FUNCTIONS.select_army("select")

      elif smart_action == ACTION_ATTACK:
        if self.can_do(obs, _ATTACK_MINIMAP):
          return actions.FUNCTIONS.Attack_minimap("now", self.attack_coords)

      return actions.FUNCTIONS.no_op()

  #check if unit is selected in single or multi-select
  def unit_type_is_selected(self, obs, unit_type):
    if (len(obs.observation.single_select) > 0  and
        obs.observation.single_select[0].unit_type == unit_type):
        return True

    if (len(obs.observation.multi_select) > 0 and
        obs.observation.multi_select[0].unit_type == unit_type):
        return True

    return False

  #Get unit by type
  def get_units_by_type(self, obs, unit_type):
    return [unit for unit in obs.observation.feature_units
      if unit.unit_type == unit_type]

  #Check if action is available to us
  def can_do(self, obs, action):
    return action in obs.observation.available_actions

# Stolen from https://github.com/MorvanZhou/Reinforcement-learning-with-tensorflow
class QLearningTable:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.9):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)

    def choose_action(self, observation):
        self.check_state_exist(observation)
        
        if np.random.uniform() < self.epsilon:
            # choose best action
            state_action = self.q_table.ix[observation, :]
            
            # some actions have the same value
            state_action = state_action.reindex(np.random.permutation(state_action.index))
            
            action = state_action.idxmax()
        else:
            # choose random action
            action = np.random.choice(self.actions)
            
        return action

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        self.check_state_exist(s)
        
        q_predict = self.q_table.ix[s, a]
        q_target = r + self.gamma * self.q_table.ix[s_, :].max()
        
        # update
        self.q_table.ix[s, a] += self.lr * (q_target - q_predict)

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(pd.Series([0] * len(self.actions), index=self.q_table.columns, name=state))